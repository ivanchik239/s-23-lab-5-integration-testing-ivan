# Lab5 -- Integration testing

## My specs

```
Budet car price per minute = 17
Luxury car price per minute = 46
Fixed price per km = 12
Allowed deviations in % = 17
Inno discount in % = 19
```

## Equivalence classes

| Argument         | Classes                           |
|------------------|-----------------------------------|
| type             | 'budget', 'luxury', nonsense      |
| plan             | 'minute', 'fixed_price', nonsense |
| distance         | <=0, >0, nonsense                 |
| planned_distance | <=0, >0, nonsense                 |
| time             | <=0, >0, nonsense                 |
| planned_time     | <=0, >0, nonsense                 |
| inno_discount    | 'yes', 'no', nonsense             |

## Decision table

| Conditions(inputs)    | R1     | R2         | R2        | R3      |         |         |         |          | R4        |
|-----------------------|:-------|------------|-----------|---------|:--------|:--------|:--------|:---------|-----------|
| type                  | budget | nonsense   | budget    | luxury  | luxury  | luxury  | luxury  | luxury   | *         |
| plan                  | minute | minute     | nonsense  | minute  | minute  | minute  | minute  | minute   | *         |
| distance              | 111    | >0         | >0        | <=0     | >0      | >0      | >0      | >0       | <=0       |
| planned_distance      | 222    | >0         | >0        | >0      | <=0     | >0      | >0      | >0       | <=0       |
| time                  | 25     | >0         | >0        | >0      | >0      | <=0     | >0      | >0       | <=0       |
| planned_time          | 50     | >0         | >0        | >0      | >0      | >0      | <=0     | >0       | <=0       |
| inno_discount         | no     | no         | no        | no      | no      | no      | no      | nonsense | nonsense  |
| --------------------- | ------ | ---------- | --------- | ------- | ------- | ------- | ------- | -------  | --------- |
| Invalid Request       |        | X          | X         | X       | X       | X       | X       | X        | X         |
| 200                   | X      |            |           |         |         |         |         |          |           |

## Test cases

| Test case | type     | plan        | distance | planned_distance | time | planned_time | inno_discount | Expected result | Actual result      |
|-----------|----------|:------------|:---------|:-----------------|:-----|:-------------|:--------------|-----------------|:-------------------|
| 1         | budget   | minute      | 111      | 222              | 25   | 50           | no            | 425             | 297.5              |
| 2         | budget   | minute      | 111      | 222              | 25   | 50           | yes           | 344.25          | 297.5              |
| 3         | budget   | fixed_price | 111      | 111              | 25   | 25           | no            | 1332            | 1387.5             |
| 4         | budget   | fixed_price | 100      | 1                | 100  | 1            | no            | 1700            | 1666.6666666666667 |
| 5         | budget   | minute      | 10       | 10               | 20   | 20           | nonsense      | Invalid Request | Invalid Request    |
| 6         | nonsense | minute      | 10       | 10               | 20   | 20           | no            | Invalid Request | Invalid Request    |
| 7         | budget   | nonsense    | 10       | 10               | 20   | 20           | no            | Invalid Request | Invalid Request    |
| 8         | budget   | minute      | 10       | 10               | 20   | 20           | *             | Invalid Request | Invalid Request    |
| 9         | budget   | fixed_price | *        | *                | 20   | *            | no            | Invalid Request | 333.3333333333333  |
| 10        | budget   | fixed_price | 10       | 10               | *    | 20           | no            | Invalid Request | null               |

## Bugs

- incorrect price calculation for both minute and fixed_price plans
- inno discount does not work
- distance, planned_distance and planned_time is not required
- missing time returns null